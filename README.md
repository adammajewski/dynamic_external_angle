
# See also
* [Computing external dynamic ray using Newton method. C console program](https://gitlab.com/c_files/dynamic_ray_newton)
* 


# Images 

## Meaning of colors
* interior is white
* exterior: color is proportional to external angle in turns. angle 0 is black ( = 0 ) and angle near 1 is almost white 254 
* target set (exterior of circle with radius = escape radius adn centered in the origin) is marked by white circle 


## final phase or argument of final z ( with bailout test)
Image was made with 
* [phase.c](phase.c)


![phase](phase.png)  


Animation:

![](phase_gif.gif)  

Image was made with 
* [phase_gif.c](phase_gif.c)
* [phase_gif.sh](phase_gif.sh) and [Image Magic](https://www.imagemagick.org/script/convert.php)

In the right up corner each frame one can see a number wich shows maximal number of iterations for this frame.

See :
* 2 white circles : bigger = circle with escape radius, smaller = boundary of Julia set
* [level sets](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/def_cqp#Level_set) of escape time 
* as IterationMax increases then:
  * number of radial bands inside increases, then black spot appears ( probably numerical error) and fills whole Julia set
  * each level set is divided into  2^i subsets = cells  

$`L_n(z_0)= \{z: |f^n_c(z_0)| \le ER  \} `$

where:
* ER = Escape Radius

## adjusted phase or compute external angle for c=0
Image was made with 
* [phase_a.c](phase_a.c)

![adjusted phase](phase_a.png)  


Image was made with 
* [phase_a2.c](phase_a2.c)

![adjuusted phase 2](phase_a2.png)  

## adjusted phase = winding number ( external angle) for c diffferent  

to do 


## final phase after fixed number of iterations without bailout test 



![](f150.gif)  

Image was made with 
* [phase_f.c](phase_f.c)
* [f.sh](f.sh) and [Image Magic](https://www.imagemagick.org/script/convert.php)

In the right up corner each frame one can see a number wich shows maximal number of iterations for this frame.

compare it with 
* [Mandel image by GONZALO E. MENA](https://gomena.github.io/gifs/fractals1/) where at each iteration (frame of the gif) different colors correspond to the angle of the iterations at each point
* Rabbit image with external rays made with [program mandel by Wolf Jung](http://www.mndynamics.com/indexp.html): 

![](rabbit.png)


Here external rays for angles 0 and 1/2 are visible

Look at first frame of above animation labelled with 0. Here is horizontal line from from right border
of the image toward origin ( here center of main component). See how this curve is changin when number of iterations grows.
it is moving to the center of smaller image to the right and with each iteration it is similar to ray 0.






```c
// from phase_f.c
// forward iteration with fixed number of iterations
// without bailout test
for  (i=0;  i<IterationMax; i++)
	Z=Z*Z+C; // forward iteration 
  
      	tf =  GiveTurn(Z); // compute angle in turns of final z = final turn = tf
  	GiveGrayColor(tf,k,data);  // color pixel proportionaly to tf
```




# Algorithm


## How angle of z  is changing  after n iterations? 


$`z_n = f_c^n(z_0)`$

These are two operation : 
* complex squaring 
* adding two complex numbers
which are repeatedly applied ( iteration)

### squaring of complex number
First check case c=0 so complex squaring map: 


[Squaring of complex number z](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/q-iterations#Forward_iteration) :
* doubles (modulo 1) it's argument $`\theta`$
* squares it's radius r

so in exponential form: 

$`f_0(z) = z^2 = (r e^{i \theta})^2 = r^2  e^{i 2 \theta}\,`$

### addind two complex numbers 
>>>

[One has 2  complex numbers: ](https://math.stackexchange.com/questions/1365622/adding-two-polar-vectors):
* $`v_1`$ with angle $`\theta_1`$, and  radius $`r_1`$
* $`v_2`$ with angle $`\theta_2`$, and  radius $`r_2`$

One have to compute angle of the sum 

  $`v_3 = v_1 + v_2`$ 
  
with:
* angle $`\theta_3`$
* radius $`r_3`$

find the difference between the angles $`\theta_2`$ and $`\theta_1`$: 

$`\alpha = \theta_2 - \theta_1 + 2n\pi`$   

where $`n`$ is an integer chosen so that $`-\pi \leq \alpha \leq \pi`$.  


$`u = r_2 \cos\alpha`$  

$`v = r_2 \sin\alpha`$  

Then, using the Law of Cosines, set
$`r_3 = \sqrt{r_1^2 + r_2^2 + 2 r_1 r_2 \cos\alpha}`$

(This formula uses $`+`$ where the usual formula uses $`-`$ because $`\alpha`$ is
an exterior angle of the triangle, rather than the interior angle used in the usual formula.)  

You now have $`r_3`$.

   
Now if $`\beta`$ is the difference between the directions of $`v_3`$ and $`v_1`$, measured in the direction that gives the angle of the smallest possible magnitude  

a function `atan2(y_3, x_3)` will compute $`\theta_3`$ without requiring you to explicitly test the signs of $`x_3`$ and $`y_3`$.

$`\beta = \text{atan2}(v, r_1 + u)`$  


$`\theta_3 = \theta_1 + \beta`$  

>>>
Description by David K





## How to compute external angle?

[Algorithm](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/boettcher#ArgPhi_-_External_angle_-_angular_component_of_complex_potential)
* on the dynamic z-plane for c not equal to zero
  * take z 
  * check if it escapes = from exterior of filled Julia set
  * if yes: iterate z ( forward iteration  f_c(z) ) until it will be near infinity
  * remember number of final iteration when $`abs(z_n) > R_{limit} `$
  * switch plane to z-plane for c=0
* on the dynamic z-plane for c equal to zero
  * make n backward iterations 
  * angle of last z is aproximation of external angle 
  
  
  
Near infinity angle of z is equal to external angle of z so we can switch the plane

$`arg_c(z) = arg(\Phi_c(z))`$

where $`\Phi_c`$ is [the Boettcher map ](https://en.wikipedia.org/wiki/External_ray#Dynamical_plane_.3D_z-plane)



# color 

It is [24 bit = RGB color](https://en.wikipedia.org/wiki/RGB_color_model). The component values are often stored as integer numbers in the range 0 to 255, the range that a single 8-bit byte can offer 



## color gradient

[Gradient](https://en.wikibooks.org/wiki/Color_Theory/Color_gradient#Gradient_functions)
* Linear 
* 1D gradient 
* with 1 segment 


```c
void GiveGrayColor(double position , int k, unsigned char c[]) 
{
  unsigned char b = 255*position;
  c[k]   = b;
  c[k+1] = b;
  c[k+2] = b;

}
```


# Functions for computing argument or (phase) angle of complex numbers

Functions
 GiveTurn(z) ->  carg(z) ->  atan2 -> atan

## GiveTurn

Computes angle of complex number in turns 
* [wikipedia : Turn(geometry)](https://en.wikipedia.org/wiki/Turn_(geometry)) 
* [wikibooks](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/def_cqp#Angle)

```c
double GiveTurn(double complex z)
{
  double argument;
  
  argument = carg(z); //   argument in radians from -pi to pi
  
  if (argument<0.0) 
    argument = TwoPi + argument ; //   argument in radians from 0 to 2*pi
     
  return (argument/TwoPi) ; // argument in turns from 0.0 to 1.0
}
```








# technical note
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)

